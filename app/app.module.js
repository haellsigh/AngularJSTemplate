/* global angular */
'use strict';

var ngApp = angular.module('ngApp', [
    'ngControllers',
    'ui.router',
    'ui.bootstrap'/*,
    'testServices'*/
]);