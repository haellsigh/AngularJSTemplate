/* global angular */
/* global ngApp */
'use strict';

/* App Module */

var ngApp = angular.module('ngApp', [
    'ngControllers',
    'ui.router',
    'ui.bootstrap'/*,
    'testServices'*/
]);

ngApp.config(['$stateProvider', '$urlRouterProvider',
  function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider
      .otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        template: '<h1>Hello, world!</h1>',
        controller: 'HomeCtrl'
      })
    
      .state('tests', {
        url: '/tests',
        template: '<h1>{{Title}}<small>{{subTitle}}</small></h1>',
        controller: 'TestListCtrl'
      });
    
    /*$routeProvider.
      when('/tests', {
      templateUrl: 'partials/test-list.html',
      controller: 'TestListCtrl'
        }).
      when('/tests/:testId', {
      templateUrl: 'partials/test-detail.html',
      controller: 'TestDetailCtrl'
        }).
      otherwise({
      redirectTo: '/tests'
        });*/
  }]);
