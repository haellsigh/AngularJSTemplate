'use strict';

/* Controllers */

var ngControllers = angular.module('ngControllers', ['ui.bootstrap']);

ngControllers.controller('TestListCtrl', ['$scope',
    function ($scope) {
        $scope.Title = "TestHeading";
        $scope.subTitle = "TestSubTitle";
    }
]);

ngControllers.controller('TestDetailCtrl', ['$scope', '$routeParams',
    function ($scope, $routeParams) {

    }
]);